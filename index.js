const express = require('express');
const routes = require('./routes');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config({ path: 'variables.env' });

//conectar mongo
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
});

//creando servidor
const app = express();

//habilitar archivos estaticos
app.use(express.static('uploads'));

//config option CORS whitelist

const whiteList = [process.env.FRONTEND_URL];
const corsOptions = {
  origin: (origin, callback) => {
    //revisar peticion; si viene de nuestra lista
    console.log(origin)
    const existe = whiteList.some((res) => res === origin);
    console.log(existe)
    if (existe) {
      callback(null, true);
    } else {
      callback(new Error('Error en CORS'));
    }
  },
};

//habilito cors para permitir intercambio de recursos entre cliente y servidor
app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

//rutas de la app
app.use('/', routes());

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 5000;

app.listen(port, host, () => {
  console.log('El servidor esta funcionando');
});
