const express = require('express');
const router = express.Router();
const clienteController = require('../controllers/clienteController');
const productoController = require('../controllers/productosController');
const pedidoController = require('../controllers/pedidosController');
const usuarioController = require('../controllers/usuarioController');
const auth = require('../middleware/auth');

module.exports = function () {
  //** Clintes
  //agregar cliente via post
  router.post('/cliente', auth, clienteController.nuevoCliente);
  //recuperar todos los clientes
  router.get('/cliente', auth, clienteController.obtenerClientes);
  //recuperar cliente por id
  router.get('/cliente/:idCliente', auth, clienteController.obtenerCliente);
  //actualizar cliente por id
  router.put('/cliente/:idCliente', auth, clienteController.actualizarCliente);
  //eliminar cliente por id
  router.delete('/cliente/:idCliente', auth, clienteController.eliminarCliente);

  //** productos */

  router.post(
    '/productos',
    auth,
    productoController.subirArchivo,
    productoController.nuevoProducto
  );
  //mostrar productos
  router.get('/productos', auth, productoController.buscarProductos);
  //mostrar producto por id
  router.get(
    '/productos/:idProducto',
    auth,
    productoController.buscarProductoId
  );
  //actualizar producto
  router.put(
    '/productos/:idProducto',
    auth,
    productoController.subirArchivo,
    productoController.actualizarProducto
  );
  //eliminar producto
  router.delete(
    '/productos/:idProducto',
    auth,
    productoController.eliminarProduto
  );
  router.post(
    '/productos/busqueda/:query',
    auth,
    productoController.buscarProducto
  );

  //** Pedidos */
  router.post('/pedidos/nuevo/:idUsuario', auth, pedidoController.nuevoPedido);
  router.get('/pedidos', auth, pedidoController.mostrarPedidos);
  router.get('/pedidos/:idPedido', auth, pedidoController.mostrarPedido);
  router.put('/pedidos/:idPedido', auth, pedidoController.actualizarPedido);
  router.delete('/pedidos/:idPedido', auth, pedidoController.eliminarPedido);

  //**Usuario */

  router.post('/crear-cuenta', usuarioController.registrarUsuario);
  router.post('/iniciar-sesion', usuarioController.iniciarSesion);

  return router;
};
