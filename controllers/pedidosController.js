const Pedidos = require('../models/Pedidos');

exports.nuevoPedido = async (req, res, next) => {
  const pedido = new Pedidos(req.body);
  try {
    await pedido.save();
    res.json({ mensaje: 'Se agrego pedido' });
  } catch (error) {
    console.log(error);
    next();
  }
};

exports.mostrarPedidos = async (req, res, next) => {
  try {
    const pedidos = await Pedidos.find({})
      .populate('cliente')
      .populate('productop');
    if (pedidos) res.json(pedidos);
  } catch (error) {
    console.log(error);
    next();
  }
};

exports.mostrarPedido = async (req, res, next) => {
  const pedido = await Pedidos.findById(req.params.idPedido)
    .populate('cliente')
    .populate({
      path: 'pedido.producto',
      model: 'Productos',
    });
  if (pedido) {
    res.json(pedido);
  } else {
    res.json({ mensaje: 'No se pudo mostrar pedido' });
    next();
  }
};

exports.actualizarPedido = async (req, res, next) => {
  const pedido = await Pedidos.findByIdAndUpdate(
    { _id: req.params.idPedido },
    req.body,
    {
      new: true,
    }
  )
    .populate('cliente')
    .populate({
      path: 'pedido.producto',
      model: 'Productos',
    });

  if (pedido) {
    res.json(pedido);
  } else {
    res.json({ mensaje: 'No se pudo actulizar pedido' });
    next();
  }
};

exports.eliminarPedido = async (req, res, next) => {
  try {
    await Pedidos.findByIdAndDelete({ _id: req.params.idPedido });
    res.json({ mensaje: 'se elimino pedido' });
  } catch (error) {
    console.log(error);
    next();
  }
};
