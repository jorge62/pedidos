const Productos = require("../models/Productos");
const multer = require("multer");
const shortid = require("shortid");

const configuracionMulter = {
  storage: (fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, __dirname + "../../uploads/");
    },
    filename: (req, file, cb) => {
      const extension = file.mimetype.split("/")[1];
      cb(null, `${shortid.generate()}.${extension}`);
    },
  })),
  fileFilter(req, file, cb) {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
      cb(null, true);
    } else {
      cb(new Error("Formato No válido"));
    }
  },
};

// pasar la configuración y el campo
const upload = multer(configuracionMulter).single("imagen");

exports.subirArchivo = (req, res, next) => {
  upload(req, res, function (e) {
    if (e) res.json({ mensaje: e });
    return next();
  });
};

exports.nuevoProducto = async (req, res, next) => {
  const producto = new Productos(req.body);

  try {
    if (req.file.filename) {
      producto.imagen = req.file.filename;
    }
    await producto.save();
    res.json({ mensaje: "se agrego nuevo producto" });
  } catch (e) {
    console.log(e);
    next();
  }
};

exports.buscarProductos = async (req, res, next) => {
  try {
    const productos = await Productos.find({});
    res.json(productos);
  } catch (error) {
    console.log(error);
    next();
  }
};

exports.buscarProductoId = async (req, res, next) => {
  const producto = await Productos.findById(req.params.idProducto);
  if (!producto) {
    res.json({ mensaje: "No hay producto" });
    return next();
  }
  res.json(producto);
};

exports.actualizarProducto = async (req, res, next) => {
  try {
    const productoNuevo = req.body;
    if (req.file) {
      productoNuevo.imagen = req.file.filename;
    } else {
      let productoAnterior = await Productos.findById(req.params.idProducto);
      productoNuevo.imagen = productoAnterior.imagen;
    }

    const producto = await Productos.findByIdAndUpdate(
      { _id: req.params.idProducto },
      productoNuevo,
      {
        new: true,
      }
    );
    res.json(producto);
  } catch (error) {
    console.log(error);
    next();
  }
};

exports.eliminarProduto = async (req, res, next) => {
  try {
    await Productos.findByIdAndDelete(req.params.idProducto);
    res.json({ mensaje: "Se elimino el producto" });
  } catch (error) {
    console.log(error);
    next();
  }
};

exports.buscarProducto = async (req, res, next) => {
  try {
    const { query } = req.params;
    const producto = await Productos.find({ nombre: new RegExp(query, "i") });
    res.json(producto);
  } catch (error) {
    console.log(error);
    next();
  }
};
