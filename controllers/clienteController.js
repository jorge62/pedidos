const Clientes = require("../models/Clientes");

// agregar un nuevo clinte

exports.nuevoCliente = async (req, res, next) => {
  //agregar cliente
  const cliente = new Clientes(req.body);

  try {
    await cliente.save();
    res.json("se agrego cliente");
  } catch (error) {
    res.send(error);
    next();
  }
};

exports.obtenerClientes = async (req, res, next) => {
  //obtener todos los clientes
  try {
    const clientes = await Clientes.find({});
    res.json(clientes);
  } catch (error) {
    console.log(error);
    next();
  }
};

exports.obtenerCliente = async (req, res, next) => {
  //obtener cliente por id
  try {
    const cliente = await Clientes.findById(req.params.idCliente);
    res.json(cliente);
  } catch (error) {
    res.json({ mensaje: "No se encontro cliente" });
    next();
  }
};

exports.actualizarCliente = async (req, res, next) => {
  //actualizar cliente por id
  try {
    const cliente = await Clientes.findByIdAndUpdate(
      { _id: req.params.idCliente },
      req.body,
      {
        new: true,
      }
    );
    res.send(cliente);
  } catch (error) {
    res.send("No se actualiza el cliente");
    next();
  }
};

exports.eliminarCliente = async (req, res, next) => {
  //actualizar cliente por id
  try {
    await Clientes.findByIdAndDelete(req.params.idCliente);
    res.json({ mensaje: "Se elimino cliente" });
  } catch (error) {
    res.json({ mensaje: "No se elimina cliente" });
    next();
  }
};
