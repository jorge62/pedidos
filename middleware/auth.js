const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization');

  if (!authHeader) {
    const error = new Error('No esta autorizado, no hay JWT');
    error.statusCode = 401;
    throw error;
  }

  //recuperar el token y verificar
  const token = authHeader.split(' ')[1];
  try {
    verificarToken = jwt.verify(token, 'LLAVESECRETA');
  } catch (error) {
    error.statusCode = 500;
    throw error;
  }

  if (!verificarToken) {
    const error = new Error('No autenticado');
    error.statusCode = 401;
    throw error;
  }

  next();
};
